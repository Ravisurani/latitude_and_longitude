package com.example.demo_api_project;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    EditText edt_east, edt_north;
    TextView tv_latitude, tv_longitude;
    Button btn_search;

    String str_east, str_north;

    ProgressDialog progressDialog;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt_east = (EditText) findViewById(R.id.edt_east);
        edt_north = (EditText) findViewById(R.id.edt_north);

        tv_latitude = (TextView) findViewById(R.id.tv_latitude);
        tv_longitude = (TextView) findViewById(R.id.tv_longitude);

        btn_search = (Button) findViewById(R.id.btn_search);

        progressDialog = new ProgressDialog(this);

        requestQueue = Volley.newRequestQueue(this);

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                str_east = edt_east.getText().toString().trim();
                str_north = edt_north.getText().toString().trim();
                get_data();

            }
        });

    }

    void get_data() {
        progressDialog.setMessage("Please wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        String url = "https://api.getthedata.com/bng2latlong/" + str_east + "/" + str_north;

        Log.d("requesturl", "get_data: " + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("requesturl", "get_data: onResponse" +response);
                        try {
                            if (response.getString("status").equals("ok")) {
                                Log.d("requesturl", "get_data: onResponse" +response.getInt("latitude"));
                                Log.d("requesturl", "get_data: onResponse" +response.getInt("longitude"));

                                tv_latitude.setText(String.valueOf(response.getString("latitude")));
                                tv_longitude.setText(String.valueOf(response.getString("longitude")));
                                progressDialog.dismiss();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(MainActivity.this, "East Or North value must be within range", Toast.LENGTH_SHORT).show();
                            }
                            progressDialog.hide();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("requesturl", "get_data: onResponse" +error);

                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Server Not Responding", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

}
