package com.example.demo_api_project;

import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("easting")
	private int easting;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("northing")
	private int northing;

	@SerializedName("status")
	private String status;

	@SerializedName("longitude")
	private double longitude;

	public int getEasting(){
		return easting;
	}

	public double getLatitude(){
		return latitude;
	}

	public int getNorthing(){
		return northing;
	}

	public String getStatus(){
		return status;
	}

	public double getLongitude(){
		return longitude;
	}
}